Return all groups

```
http  "https://gitlab.com/api/v3/groups?per_page=100" "PRIVATE-TOKEN:psdeoFXxX8Fz4GgC9HXE"
```

Return all projects in group = 2503990 (tol-project)

```
http  "https://gitlab.com/api/v3/groups/2503990/projects?per_page=100" "PRIVATE-TOKEN:psdeoFXxX8Fz4GgC9HXE"
```


Return all proyects in namespace tol-project
```
http  "https://gitlab.com/api/v3/projects?namespace_id=2503990&visibility=private&simple=true&per_page=100" "PRIVATE-TOKEN:psdeoFXxX8Fz4GgC9HXE"
```

Set a probject visibility to true
```
http PUT "https://gitlab.com/api/v3/projects/5692596?public=true" "PRIVATE-TOKEN:Zf7jVV2uqGg4-_QfxDJ5"
```
