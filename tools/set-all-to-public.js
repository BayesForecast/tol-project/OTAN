const request = require('request-promise-native');

const userToken = 'Zf7jVV2uqGg4-_QfxDJ5';

function setToPublic(project) {
  const options = {
    method: 'PUT',
    uri: `https://gitlab.com/api/v3/projects/${project.id}`,
    qs: {
      public: true
    },
    headers: {
      'PRIVATE-TOKEN': userToken
    },
    json: true // Automatically parses the JSON string in the response
  };
  return request(options);
}

function getAllPrivateProjects() {
  const options = {
    uri: 'https://gitlab.com/api/v3/groups/2503990/projects',
    qs: {
      simple: true,
      visibility: 'private',
      per_page: 100
    },
    headers: {
      'PRIVATE-TOKEN': userToken
    },
    json: true // Automatically parses the JSON string in the response
  };
  return request(options);
}

const allProjects = getAllPrivateProjects();

allProjects
  .then((projects) => {
    console.log('User has %d repos', projects.length);
    const actions = projects.map(setToPublic);

    return Promise.all(actions);
  })
  .then(console.log)
  .catch((err) => {
    // API call failed...
  });
