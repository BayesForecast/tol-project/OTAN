# This module defines
# TOL_FOUND.  If false, you cannot build anything that requires TOL.
# TOL_INCLUDE_DIR where to find the tol's header files
# TOL_LIBRARIES, the libraries to link against to use TOL
# also defined, but not for general use is
# TOL_LIBRARY, the path to tol library.
#
# You can provide in TOL_PREFIX_PATH the path to the prefix
# instalation of TOL.
#

include( LibFindMacros )

set( TOL_FOUND 0 )

if( EXISTS ${TOL_PREFIX_PATH} )

  #find includes
  find_path(
    TOL_INCLUDE_DIR
    NAMES "tol/tol_bcommon.h"
    PATHS ${TOL_PREFIX_PATH}
    PATH_SUFFIXES "include"
    NO_DEFAULT_PATH )
  find_library( TOL_LIBRARY
    NAMES tol
    PATHS ${TOL_PREFIX_PATH}
    PATH_SUFFIXES "lib"
    NO_DEFAULT_PATH )
else(  EXISTS ${TOL_PREFIX_PATH} )

  #find includes
  find_path(
    TOL_INCLUDE_DIR
    NAMES "tol/tol_bcommon.h" )
  find_library( TOL_LIBRARY
    NAMES tol )
endif( EXISTS ${TOL_PREFIX_PATH} )

set( TOL_PROCESS_INCLUDES TOL_INCLUDE_DIR )
set( TOL_PROCESS_LIBS TOL_LIBRARY )
libfind_process( TOL )

if( TOL_FOUND )
  if( EXISTS ${TOL_PREFIX_PATH} )
    set( CMAKE_PREFIX_PATH "${TOL_PREFIX_PATH}" ${CMAKE_PREFIX_PATH} )
  endif( )
  add_definitions(
    -DHAVE_CONFIG_H -DUSE_DELAY_INIT -D__USE_ZIP_ARCHIVE__
    -D__USE_DYNSCOPE__
    -D__POOL_NONE__=0 -D__POOL_BFSMEM__=1 -D__USE_POOL__=__POOL_BFSMEM__
    -D__HASH_MAP_MSVC__=1 -D__HASH_MAP_GOOGLE__=2 -D__HASH_MAP_GCC__=3 
    -D__HASH_MAP_ICC__=4 -D__USE_HASH_MAP__=__HASH_MAP_GOOGLE__ )
  if( UNIX )
    add_definitions( -DUNIX )
  endif( )
endif( TOL_FOUND )

macro( add_tol_package target_name )
  add_library( ${target_name} SHARED ${ARGN} )
  set_target_properties( ${target_name} 
    PROPERTIES PREFIX "" SKIP_BUILD_RPATH TRUE )
  if( WIN32 )
    target_link_libraries( ${target_name} PRIVATE ${TOL_LIBRARIES} )
  endif( WIN32 )
  
  if( "${CMAKE_SIZEOF_VOID_P}" EQUAL "4" )
    if( UNIX )
      set( target_dir "CppTools/Linux_x86_32" )
    else( UNIX )
      if( MINGW )
        set( target_dir "CppTools/MinGW_32" )
      else( MINGW )
        set( target_dir "CppTools/Windows_x86_32" )
      endif( MINGW )  
    endif( UNIX )
  elseif ( "${CMAKE_SIZEOF_VOID_P}" EQUAL "8" )
    if( UNIX )
      set( target_dir "CppTools/Linux_64_GNU" )
    else( UNIX )
      if( MINGW )
        set( target_dir "CppTools/Windows_64_GNU" )
      else( MINGW )
        set( target_dir "CppTools/Windows_64_MSVC" )
      endif( MINGW )  
    endif( UNIX )
  else()
    set( target_dir "CppTools/Unknown" )
  endif()
  if( CMAKE_BUILD_TYPE )
    string( TOUPPER ${CMAKE_BUILD_TYPE} cmake_build_type_upper )
  else( CMAKE_BUILD_TYPE )
    set( cmake_build_type_upper "RELEASE" )
  endif( CMAKE_BUILD_TYPE )
  if(cmake_build_type_upper MATCHES DEBUG)
    set( target_dir "${target_dir}_dbg" ) 
  endif(cmake_build_type_upper MATCHES DEBUG) 
  set( variant_dir "${PROJECT_SOURCE_DIR}/${target_dir}" )
  file( MAKE_DIRECTORY ${variant_dir} )
  set( variant_dest "${variant_dir}/$<TARGET_FILE_NAME:${target_name}>" )
  message("${target_name} will be compiled to ${variant_dest}")
  # copy the library to variant dir 
  add_custom_command(TARGET ${target_name} POST_BUILD
    #COMMAND strip $<TARGET_FILE:${target_name}>
    COMMAND ${CMAKE_COMMAND} 
            -E copy $<TARGET_FILE:${target_name}> ${variant_dest} )
endmacro( add_tol_package )
