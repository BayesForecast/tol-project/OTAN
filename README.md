# OTAN = Official TOL Archive Network

This a repository containing a reference to each TOL package. Each
repository is referenced as a git submodule. In this way all
repositories can be pulled at once.

## Initializing the local repository for OTAN.

OTAN is made of many TOL packages referenced as submodule. You can
clone the complete repository with the following sentence:

```
git clone --recurse-submodules https://gitlab.com/BayesForecast/tol-project/OTAN.git
```

If you ommit the option `--recurse-submodules` then every packages is
cloned in an empty subdirectory.

In order to update a single package you have to execute:

```
git submodule update --init --remote <PackageName>
```

where `<PackageName>` is the name of the package, for instance to update
only TclCore the sentence would be:

```
git submodule update --init --remote TclCore
```

## Updating the submodules

```
git submodule update --recursive --remote
```

# How to add a new package

## Add submodule to OTAN

Let's suppose you have created a repository `git@gitlab.com:BayesForecast/tol-project/TclCore.git`.

If you want to make it a submodule of OTAN you have to execute the
following sentence inside a local copy of OTAN:

```
git submodule add git@gitlab.com:BayesForecast/tol-project/TclCore.git TclCore
```

Following https://docs.gitlab.com/ce/ci/git_submodules.html it is
recommended to edit the file `.gitmodules` to change the package
repository from absolute reference to a relative one.

In the previous example `TclCore` should appears in the file
`.gitmodules` as:

```
[submodule "TclCore"]
	path = TclCore
	url = git@gitlab.com:BayesForecast/tol-project/TclCore.git
```

and should be changed to:

```
[submodule "TclCore"]
	path = TclCore
	url = ../TclCore.git
```

# Building a package

A TOL package can be built and uploaded to the repository using the
package TolPkg.

## How to build a package in OTAN


```c++
#Require TolPkg;

Real TolPkg::Builder::AppendRepository(
  "DB.OTAN",
  NameBlock DBConnect::Obtain("bysforofitol")
  );

Text source = "GITLAB.OTAN";
Real TolPkg::Builder::AppendSource(
  source, "GIT",
  "https://gitlab.com/BayesForecast/tol-project/OTAN.git", [[
      Real useSubmodule = True
      ]]);

Text package = "TclCore";

Real overwrite = 0;
Real TolPkg::Builder::ProducePackage(package, source, ?, "DB.OTAN", overwrite);
```

## How to build a package not in OTAN

```c++
#Require TolPkg;

Real TolPkg::Builder::AppendRepository(
  "DB.OTAN",
  NameBlock DBConnect::Obtain("bysforofitol")
  );

Text source = "GITLAB.TCLCORE";
Real TolPkg::Builder::AppendSource(
  source, "GIT",
  "https://gitlab.com/BayesForecast/tol-project/TclCore.git",
  NoNameBlock);

Text package = "TclCore";

Real overwrite = 0;
Real TolPkg::Builder::ProducePackage(package, source, ?, "DB.OTAN", overwrite);
```
